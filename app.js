const express = require("express");
const bodyParser = require('body-parser');
const mysql = require('mysql2');
const ejs = require("ejs");

const app = express();

app.set('view engine', 'ejs');

app.use(bodyParser.urlencoded({extended:true}))
app.use(express.static("public"));


const connection = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: 'Gr1maldy#006!',
    database: 'absa'
});

//connect to mysql server
connection.connect(function (err){

    if(err){
        return console.error('error: ' + err.message);
    }

    console.log('Connected to Absa db successfully');
});


let insert_ids = ['2'];

//it begins here

app.get("/", function (req,res){
    res.sendFile(__dirname + "/views/index.html");
});






app.get("/rate-second", function (req, res){
    res.sendFile(__dirname + "/views/rate-second.html");
});



app.get("/rate", function (req, res){

    res.render("rate");
});




app.post("/rate", function (req,res){

    // insert_ids.pop();

    const rateType_id = req.body.rate_type;
    const code_id = req.body.currency_codes;
    const buy = req.body.buy;
    const sell = req.body.sell;

    const rate_info = [rateType_id, code_id, buy, sell];
    console.log(rate_info);

    let insert_rate = `INSERT INTO temp_rates(rateType_id, code_id, buy, sell, added)
VALUES(?,?,?,?, current_timestamp())`;

    connection.query(insert_rate, rate_info, function (err, results){

        if(err){
            return console.error(err.message);
        } else {

            //get insert id

            let insert_id = results.insertId;
            insert_ids.push(insert_id.toString());
            console.log("insert id: " + results.insertId);
            res.redirect("/confirm");

        }

    })


});



app.get("/confirm", function (req, res){


    let id = +insert_ids;
    let getRate_info = `SELECT rateType_id, rate_type, code_id, code_name, description, buy, sell FROM temp_rates INNER JOIN rate_type USING (rateType_id) INNER JOIN codes USING (code_id) WHERE rate_id=` + id;


    connection.query(getRate_info,(error, results) => {

        if (error){

            return console.error(error.message);

        } else {

            // console.log(results);
            console.log(insert_ids);
            res.render("confirm", {rateInfo: results});
        }

    });

});




app.post("/confirm", function (req, res){

    const rateType_id = req.body.rate_type;
    const code_id = req.body.currency_codes;
    const buy = req.body.buy;
    const sell = req.body.sell;


    let rate_info = [rateType_id, code_id];

    let getConfirmed_rate = `SELECT rateType_id, code_id FROM rates WHERE rateType_id = ? AND code_id = ?`;

    connection.query(getConfirmed_rate, rate_info,function (err, results){

        if(err){

            console.error(err.message);

        }

        if (results.length === 0){

            let confirmed_info = [rateType_id, code_id, buy, sell];

            let confirmed_rate = `INSERT INTO rates(rateType_id, code_id, buy, sell, added)
            VALUES(?,?,?,?, current_timestamp())`;

            connection.query(confirmed_rate, confirmed_info, function (err, results){

                if(err){

                    return console.error(err.message);
                } else {

                    console.log(results.insertId)
                }

            })


        } else {

            let confirmed_info = [buy, sell, rateType_id, code_id];

            let update_rate = `UPDATE rates SET buy = ?, sell = ?, added = current_timestamp() WHERE rateType_id = ? AND code_id = ?`;

            connection.query(update_rate, confirmed_info, function (err, results) {

                if (err) {

                    return console.error(err.message);

                } //remember to add else statement for notifications
            })


        }

        res.redirect("/rate");

    });

});




app.get("/fx-display", function (req, res){

    let transfer_info = `SELECT code_name, buy, sell FROM rates INNER JOIN rate_type USING (rateType_id) INNER JOIN codes USING (code_id) WHERE rateType_id = 2`;

    connection.query(transfer_info, (err, results) => {

        if (err) {

            return console.error(err.message);

        } else {

            console.log(results);

            res.render("fx-display", {transfer_data: results});

        }

    });

});





app.listen(3000,function (){
    console.log("listening on port 3000");
});

